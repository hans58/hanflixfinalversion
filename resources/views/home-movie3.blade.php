<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Trirong" rel="stylesheet">
    <link rel="icon" href="images/gg.png" type="image/gif" sizes="16x16">

    <!-- CSS -->
    <link href="css/livestream.css" rel="stylesheet">

</head>
<body>
    <div class="container">
        <div class="col">
            <div id="banner">
                <div class="logo" id="logo" >
                    <a href="{{ url ('home') }}">
                        <img src="images/gg.png" alt="logo" width="50px" height="50px">
                    </a>
                </div>
            </div>
            <!-- Video Player -->
            <video-js id='hanflix' class="vjs-default-skin" controls>
                <source 
                    src="https://hanflix-moviee.s3.ap-southeast-2.amazonaws.com/Burden+(2022)+%5B1080p%5D+%5BWEBRip%5D+%5B5.1%5D+%5BYTS.MX%5D/output/Burden.2022.1080p.WEBRip.x264.AAC5.1-%5BYTS.MX%5D.m3u8"
                    type="application/x-mpegURL">

                <!-- Subtitle -->
                <track kind="captions" label="English" srclang="en" src="Subs/burden_eng.vtt" default>
                <track kind="captions" label="Arabic" srclang="ar" src="Subs/daytodie_arab.vtt">
				<track kind="captions" label="French" srclang="fr" src="Subs/daytodie_french.vtt">

                </source>
            </video-js>

            <script src="https://unpkg.com/video.js/dist/video.js"></script>
            <script src="https://unpkg.com/videojs-contrib-quality-levels@2.1.0/dist/videojs-contrib-quality-levels.js"></script>
            <script src="https://unpkg.com/videojs-http-source-selector/dist/videojs-http-source-selector.js"></script>

        </div>
    </div>




    <script>
            var options = {
                plugins: {
                httpSourceSelector:
            {
            default: 'auto'
             }
          }
      };
            var player = videojs('hanflix', options);
            player.httpSourceSelector();
             var player = videojs('hanflix', {autoplay: true});
        // videojs('hanflix', {autoplay: true});
        player.autoplay('hanflix');
    </script> 



  

</body>
</html>
