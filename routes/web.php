<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\GenreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/email', [AuthController::class, 'email']);

Route::get('/', function () {
    return view('landing');
});

Route::get('/livestream', function () {
    return view('livestream');
});

Route::get('/livestream2', function() {
    return view('livestream2');
});

Route::get('/login', function () {
    return view('login');
})->middleware(['auth','verified'])->name('login');

Route::get('/home-movie', function() {
    return view('home-movie');
});

Route::get('/home-movie1', function() {
    return view('home-movie1');
});

Route::get('/home-movie2', function() {
    return view('home-movie2');
});

Route::get('/home-movie3', function() {
    return view('home-movie3');
});


Route::resource('movies', MovieController::class);

Route::get('/movies',[MovieController::class, 'index'])->name('index');
Route::get('/create',[MovieController::class, 'create'])->name('create');
Route::post('store/',[MovieController::class, 'store'])->name('store');
Route::get('show/{movie}',[MovieController::class, 'show'])->name('show');
Route::get('edit/{movie}',[MovieController::class, 'edit'])->name('edit');
Route::put('edit/{movie}',[MovieController::class, 'update'])->name('update');
Route::delete('/{movie}',[MovieController::class, 'destroy'])->name('destroy');



require __DIR__.'/auth.php';
